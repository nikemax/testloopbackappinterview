var Twitter = require('twitter');
var twitterAPIClient = new Twitter( 
    require('./twitter_api_conf.js')
);

var TwitterAPI = function() {
    this.twitts = [];
    
    // next - loopback/express generator for use()
    this.get = function(users, next, loopbackModelCallBack){
        var completed_requests = 0;
        
        for (var i = 0, users_count = users.length; i < users_count; i++){
            var twitts_req_params = {
                screen_name: users[i], 
                count: 3
            };
            
            var twitts = this.twitts;
            twitterAPIClient.get(
                'statuses/user_timeline', 
                twitts_req_params, 
                function(error, tweets, response){
                  completed_requests++;
                
                  if (!error) {
                    twitts.push(tweets);
                  }
                    
                  if (completed_requests == users_count) {
                    if (loopbackModelCallBack) {
                        loopbackModelCallBack(null, twitts);
                    } else {
                        next();   
                    }
                  }
                }
            );
        }
    }
}

module.exports = TwitterAPI;