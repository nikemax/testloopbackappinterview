module.exports = function(Twitts) {
    Twitts.getByUser = function(users, callback) {
        var twitterAPI = require("../../twitter_api.js");
        var twitterClientAPI = new twitterAPI();
        
        twitterClientAPI.get(users, null, callback)
    }
     
    Twitts.remoteMethod(
        'getByUser', 
        {
            description: 'Get twitts of users list',
            accepts: {arg: 'users', type: 'array', required: true},
            http: {verb: 'get', path: '/getByUser'},
            returns: {arg: 'twitts', type: 'array'}
        }
    );
    
    Twitts.sharedClass.find('create', true).shared = false;
    Twitts.sharedClass.find('findOne', true).shared = false;
    Twitts.sharedClass.find('update', true).shared = false;
};
