var loopback = require("loopback");
var twitterAPI = require("./twitter_api.js");

var app = loopback();
var twitterClient = new twitterAPI();

app.use('/twitts', function(req, res, next){
    if (req.query.user) {
        //use url ?user=nikemax2007&user=nodejs ... 
        twitterClient.get(req.query.user, next);
    } else {
        res.send('empty url GET param user');
    }
});

app.get('/twitts', function(req, res){
    res.json(twitterClient.twitts);
});
app.listen(3456);